window.onload = (event) => {
    let data = storage.get();
    pageState['tasksToDo'] = data ? data.tasksToDo : [];
    pageState['tasksPending'] = data ? data.tasksPending : [];
    pageState['tasksDone'] = data ? data.tasksDone : [];
    pageState['lastTaskId'] = data ? data.lastTaskId : [];

    _subscribeAll();
    _render();
    getWeather();
};

const param = {
    "url": "https://api.openweathermap.org/data/2.5/",
    "appid": "c178d99931c86a46a1a34fcde8660bb3"
}

function getWeather() {
    const cityId = 4164138;
    fetch(`${param.url}weather?id=${cityId}&units=metric&APPID=${param.appid}`)
        .then(weather => {
            return weather.json();
        }).then(res => {
            document.getElementById('city').innerHTML = res.name;
            document.getElementById('temp').innerHTML = Math.round(res.main.temp) + '&deg;';
            document.getElementById('des').innerHTML = res.weather[0]['description'];

    });
}



let pageState = {}

function create() {
    let summary = document.getElementById('summaryInput').value;
    let priority = +document.getElementById('prioritySelect').value;
    let description = document.getElementById('descriptionInput').value;

    if (!validUniqSummary(getAllTasks(), summary)) { // TASK IS CREATED
        alertValid();
        return;
    }

    pageState.tasksToDo.push({
        'id': ++pageState.lastTaskId,
        'summary': summary,
        'priority': priority,
        'description': description,
    });
}

function closeTask(id, typeTask) {
    _removeTaskById(id, pageState[typeTask])
}

function _sortTasks(a) {
    a.sort((a, b) => a.priority < b.priority ? 1 : -1);
}

function _render() {
    _renderToDo();
    _renderPending();
    _renderDone();
    _subscribeDragElements();
}

function _renderToDo() {
    let content = '';
    let footerTasksAmount = document.getElementById('countToDo');
    _sortTasks(pageState.tasksToDo);

    pageState.tasksToDo.forEach((task) => {
        content += _getTaskTemplate({
            'task': task,
            'removeFrom': 'tasksToDo',
            'fromTasks': 'tasksToDo',
            'toTasks': 'tasksPending'
        })
    });

    document.getElementById('containerToDo').innerHTML = content;
    if (pageState.tasksToDo.length == 1 || pageState.tasksToDo.length == 0) {
        footerTasksAmount.innerHTML = pageState.tasksToDo.length + ' item';
    } else footerTasksAmount.innerHTML = pageState.tasksToDo.length + ' items';
}

function _renderPending() {
    let content = '';
    let footerTasksAmount = document.getElementById('countPending');
    _sortTasks(pageState.tasksPending);

    pageState.tasksPending.forEach((task) => {
        content += _getTaskTemplate({
            'task': task,
            'removeFrom': 'tasksPending',
            'fromTasks': 'tasksPending',
            'toTasks': 'tasksDone'
        })
    });

    document.getElementById('containerPending').innerHTML = content;
    if (pageState.tasksPending.length == 1 || pageState.tasksPending.length == 0) {
        footerTasksAmount.innerHTML = pageState.tasksPending.length + ' item';
    } else footerTasksAmount.innerHTML = pageState.tasksPending.length + ' items';
}

function _renderDone() {
    let content = '';
    let footerTasksAmount = document.getElementById('countDone');


    pageState.tasksDone.forEach((task) => {
        content += _getTaskTemplate({
            'task': task,
            'removeFrom': null,
            'fromTasks': null,
            'toTasks': null
        })
    });

    document.getElementById('containerDone').innerHTML = content;
    if (pageState.tasksDone.length == 1 || pageState.tasksDone.length == 0) {
        footerTasksAmount.innerHTML = pageState.tasksDone.length + ' item';
    } else footerTasksAmount.innerHTML = pageState.tasksDone.length + ' items';
}

function _getTaskTemplate(settings){
    let template = '';

    if (settings['removeFrom']) {
        template += `<div class="task border border-2 p-2 position-relative mt-2" id="${settings['task']['id']}" draggable="true">`;
    } else {
        template += `<div class="task border border-2 p-2 position-relative mt-2" id="${settings['task']['id']}"`;
    }
    
    if (settings["removeFrom"]) {
        template += `<button 
                        type="button" 
                        onClick='closeTask(${settings['task']['id']}, "${settings['removeFrom']}")' 
                        class="btn-close position-absolute top-0 end-0 m-2" 
                        aria-label="Close">                    
                     </button>`;
    }

    template += ` <div id="taskContent" class="mt-4 mb-5">
                    <h5 class="card-title">${settings['task']['summary']}</h5>
                    <p class="card-text">${mapValueToPriority[settings['task']['priority']]}</p>
                    <p class="card-text">${settings['task']['description']}</p>
                  </div>`
    
    template += '</div>';
    
    return template;
}

function _removeTaskById(taskId, tasksArr) {
    let indexOfTask = tasksArr.findIndex(task => task.id === taskId);
    if (indexOfTask !== -1) {
        tasksArr.splice(indexOfTask, 1);
    }
}

function _moveTask(idTask, fromArr, toArr) {
    let index = fromArr.findIndex(item => item.id == idTask);
    toArr.push(fromArr[index]);
    fromArr.splice(index, 1);
}

function _subscribeArrayOnRender(array, render) {
    Object.defineProperty(array, "push", {
        enumerable: false, // hide from for...in
        configurable: false, // prevent further meddling...
        writable: false, // see above ^
        value: function (...args)
        {
            let result = Array.prototype.push.apply(this, args);
            render();
            _subscribeDragElements()
            storage.set(pageState);
            return result; // Original push() implementation
        }
    });

    Object.defineProperty(array, "splice", {
        enumerable: false, // hide from for...in
        configurable: false, // prevent further meddling...
        writable: false, // see above ^
        value: function (...args)
        {
            let result = Array.prototype.splice.apply(this, args);
            render();
            _subscribeDragElements()
            storage.set(pageState);
            return result; // Original push() implementation
        }
    });
}

function _subscribeAll() {
    _subscribeArrayOnRender(pageState.tasksToDo, _renderToDo);
    _subscribeArrayOnRender(pageState.tasksPending, _renderPending);
    _subscribeArrayOnRender(pageState.tasksDone, _renderDone);
}

function _onDragStart(e) {
    this.style.opacity = '0.4';
    e.dataTransfer.setData("containerId", e.target.parentNode.dataset.array);
    e.dataTransfer.setData("taskId", e.target.id)
}

function _onDragEnd(e) {
    this.style.opacity = '1';
}

function _onDragOver(e) {
    e.preventDefault();
}

function _onDrop(e) {
    e.preventDefault();
    let prevContainerId = e.dataTransfer.getData("containerId");
    let currentContainerId = e.target.dataset.array || e.currentTarget.dataset.array;
    let taskId = +e.dataTransfer.getData("taskId");
    _moveTask(taskId, pageState[prevContainerId], pageState[currentContainerId]);
}

function _subscribeDragElements() {
    let tasks = document.querySelectorAll('.task');
    let containers = document.querySelectorAll('.card-body');
    containers.forEach(function (container) {
        container.addEventListener('dragover', _onDragOver, false);
        container.addEventListener('drop', _onDrop, false);
    });

    tasks.forEach(function (task) {
        task.addEventListener('dragstart', _onDragStart, false);
        task.addEventListener('dragend', _onDragEnd, false);
    });
}
