function validUniqSummary(tasks, input) {
    input = input.trim();

    if (tasks.length === 0 || !input) {
        return true
    }

    let notUniqTask = tasks.find(item => {
        let summary = item.summary.trim();
        return input === summary;
    })

    return notUniqTask ? false : true;
}