window.onload = (event) => {
    let data = storage.get();
    tasks = tasks.concat(data.tasksToDo, data.tasksPending, data.tasksDone);
    filteredTasks = [...tasks];
    console.log(filteredTasks)
    _render(tasks);
};
let tasks =[];
let filteredTasks;

function search() {
    let searchedTask = document.getElementById('searchTask').value;
    filteredTasks = [];

    if (searchedTask.length >= 3) {
        for (let item = 0; item < tasks.length; item++) {
            if (tasks[item].summary.toLowerCase().includes(searchedTask.toLowerCase())) {
                filteredTasks.push(tasks[item]);
            }
        }
        _render(filteredTasks);
    } else {
        _render(tasks);
    }
}

function _render(tasks) {
    let content = '';
    // let tasksAmount = document.getElementById('countTasks');

    tasks.forEach((task) => {
        content += _taskTemplate({
            'task': task,
            'removeFrom': 'tasksToDo',
            'fromTasks': 'tasksToDo',
            'toTasks': 'tasksPending'
        })
    });

    document.getElementById('tasks-list').innerHTML = content;
    // if (tasks.length == 1 || pageState.tasksToDo.length == 0) {
    //     tasksAmount.innerHTML = tasks.length + ' item';
    // } else tasksAmount.innerHTML = tasks.length + ' items';
}

function _taskTemplate(settings){
    let template = '';

    template += `<div class="col">
                    <div class="p-3 border bg-light">`;

    template += ` <div class="mt-4 mb-5">
                    <h5 class="card-title">${settings['task']['summary']}</h5>
                    <p class="card-text">${mapValueToPriority[settings['task']['priority']]}</p>
                    <p class="card-text">${settings['task']['description']}</p>
                  </div>`

    template += '</div></div>';

    return template;
}